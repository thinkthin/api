var Sequelize = require('sequelize');

var settings = {
    host: 'localhost',
    dialect: 'mysql',
    port: 3306,
    pool: {
        min: 0,
        max: 5,
        iddle: 10000
    },
    logging: false
};

// var sequelize = new Sequelize('nakoding_ppsu','nakoding_ppsu','P@ssw0rd',settings);
var sequelize = new Sequelize('ppsu','root','',settings);

module.exports = sequelize;