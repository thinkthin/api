var Joi = require('joi');

module.exports = {
    users: {
        body: {
            UserID: Joi.number().required(),
            Name: Joi.string().required(),
            Username: Joi.string().required(),
            Password: Joi.string().required(),
            Hash: Joi.string(),
            Role: Joi.string().required(),
            Status: Joi.boolean()
        }
    },
    tracks: {
        body: {
            TrackID: Joi.number().required(),
            UserID: Joi.number().required(),
            TrackName: Joi.string().required(),
            Status: Joi.boolean()
        }
    },
    trackshistories: {
        body: {
            TrackHistoryID: Joi.number().required(),
            TrackID: Joi.number().required(),
            Latitude: Joi.number().min(-180).max(180).required(),
            Longitude: Joi.number().min(-180).max(180).required(),
            Status: Joi.boolean()
        }
    },
    trackstatuses: {
        body: {
            TrackStatusID: Joi.number().required(),
            TrackID: Joi.number().required(),
            TrackStatus: Joi.string().required(),
            Status: Joi.boolean()
        }
    }
};