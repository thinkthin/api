var Joi = require('joi');

module.exports = {
    page:{
        body: {
            page: Joi.number().required(),
            pageSize: Joi.number().required(),
            criteria: Joi.array().required(),
            order: Joi.object().required()
        }
    },
    count:{
        body: {
            criteria: Joi.array().required()
        }
    }
};