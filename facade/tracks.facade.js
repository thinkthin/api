var express = require('express');
var Sequelize = require('sequelize');
var async = require('async');
var ApiResponse = require('../model/apiresponse');
var Tracks = require('../model/tracks');
var Users = require('../model/users');
var validate = require('express-validation');
var Joi = require('joi');
var reqvalid = require("../validation/request-validation");
var modelvalid = require("../validation/model-validation");

var router = express.Router();

router.get('/', (req, res) => {
    Tracks.findAll({
        include: [{
            model: Users,
            attributes: ['Name']
        }]
    }).then((res2) => {
        res.json(ApiResponse.ok('ok bos', res2));
    });
});

router.get('/:id', validate({
    params: {
        id: Joi.number().required()
    }
}), (req, res) => {
    Tracks.findById(req.params.id, {
        include: [{
            model: Users,
            attributes: ['Name']
        }]
    }).then((res2) => {
            if (res2) {
                res.json(ApiResponse.ok('ok bos', res2));
            } else {
                res.json(ApiResponse.notFound());
            }
        },
        (err) => {
            res.json(ApiResponse.unknownError('error', err));
        });
});

router.post('/paging', validate(reqvalid.page), (req, res, next) => {
    var criterias = req.body.criteria || [];
    var where = [];
    var str = "";
    criterias.forEach((item, i) => {
        str = `{"${item.criteria}":{"$like":"%${item.value}%"}}`;
        where.push(JSON.parse(str));
    });
    let page = req.body.page < 1 ? 1 : req.body.page;
    let start = (page - 1) * req.body.pageSize;

    async.parallel({
        rows: (callback) => {
            Tracks.findAll({
                include: [{
                    model: Users,
                    attributes: ['Name']
                }],
                attributes: Object.keys(Tracks.attributes).concat([
                    [Sequelize.literal('(SELECT `th`.`Latitude` FROM `trackhistories` `th` WHERE `th`.`TrackID` = `tracks`.`TrackID` ORDER BY `th`.`TrackHistoryID` DESC LIMIT 1)'), 'Latitude'],
                    [Sequelize.literal('(SELECT `th`.`Longitude` FROM `trackhistories` `th` WHERE `th`.`TrackID` = `tracks`.`TrackID` ORDER BY `th`.`TrackHistoryID` DESC LIMIT 1)'), 'Longitude'],
                    [Sequelize.literal('(SELECT `ts`.`TrackStatus` FROM `trackstatuses` `ts` WHERE `ts`.`TrackID` = `tracks`.`TrackID` ORDER BY `ts`.`TrackStatusID` DESC LIMIT 1)'), 'TrackStatus']
                ]),
                where: where,
                offset: start,
                limit: req.body.pageSize,
                order: [
                    [req.body.order.column, req.body.order.direction]
                ]
            }).then(rowData => {
                callback(null, rowData);
            }).catch(err => {
                res.json(ApiResponse.unProcessableEntity(err));
            });
        },
        rowCount: (callback) => {
            Tracks.count({
                where: where
            }).then(rowCount => {
                callback(null, rowCount);
            });
        }
    }, (err, result) => {
        if (err) {
            res.json(ApiResponse.notAcceptable(err, req.body));
        } else {
            result['pageCount'] = Math.ceil(result.rowCount / parseFloat(req.body.pageSize));;
            res.json(ApiResponse.ok('ok', result));
        }
    });
});

router.post('/', validate(modelvalid.tracks), (req, res) => {
    Tracks.create(req.body).then((res2) => {
        res.json(ApiResponse.created('ok', res2));
    }, (error) => {
        res.json(ApiResponse.unProcessableEntity('insert fail', error.errors));
    });
});

router.put('/', validate(reqvalid.count), (req, res) => {
    Tracks.findById(req.body.TrackID).then(
        (res2) => {
            if (res2) {
                res2.update(req.body).then((res3) => {
                    res.json(ApiResponse.ok('ok'));
                }, (error) => {
                    res.json(ApiResponse.unProcessableEntity('unProcessable Entity', error));
                });
            } else {
                res.json(ApiResponse.notFound());
            }
        },
        (err) => {
            res.json(ApiResponse.unknownError('error', err));
        }
    );
});

router.delete('/:id', validate({
    params: {
        id: Joi.string().min(1).max(5).required()
    }
}), (req, res) => {
    Tracks.findById(req.params.id).then(
        (res2) => {
            if (res2) {
                res2.destroy().then((res3) => {
                    res.json(ApiResponse.ok('ok'));
                }, (error) => {
                    res.json(ApiResponse.unProcessableEntity('unProcessable Entity', error));
                });
            } else {
                res.json(ApiResponse.notFound());
            }
        },
        (err) => {
            res.json(ApiResponse.unknownError('error', err));
        }
    );
});

router.post('/count', validate(reqvalid.count), (req, res, next) => {
    var criterias = req.body.criteria || [];
    var where = [];
    var str = "";
    criterias.forEach((item, i) => {
        str = `{"${item.criteria}":{"$like":"%${item.value}%"}}`;
        where.push(JSON.parse(str));
    });

    Tracks.count({
        where: where
    }).then(res2 => {
        res.json(ApiResponse.ok('ok bos', res2));
    });
});

module.exports = router;