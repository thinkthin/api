var express = require('express');
var Sequelize = require('sequelize');
var async = require('async');
var ApiResponse = require('../model/apiresponse');
var Users = require('../model/users');
var validate = require('express-validation');
var Joi = require('joi');
var reqvalid = require("../validation/request-validation");
var modelvalid = require("../validation/model-validation");
var config = require('../config/config.js');
var Jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

var router = express.Router();

router.post('/', (req, res) => {
    var auth = req.headers.authorization.replace('Basic ', '');
    if (auth) {
        var strAuth = Buffer.from(auth, 'base64').toString();

        var cred = strAuth.split(':');
        if (cred.length != 2) {
            return res.json(ApiResponse.unAuthorized('Login fail!', {
                message: 'Invalid Authorization!'
            }));
        }

        // Get param
        var username = cred[0];
        var password = cred[1];

        // Check valid user
        Users.findOne({
            where: {
                Username: username,
                Password: password
            }
        }).then(users => {
            // Load hash from your password DB.
            if (users) {
                bcrypt.compare(password, users.Hash, (err2, res2) => {
                    if (res2) {
                        try {
                            var token = Jwt.sign(config.payLoad, config.secretKey);
                            res.json(ApiResponse.ok('Login success!', {
                                token: token,
                                users: users
                            }));
                        } catch (ex) {
                            res.json(ApiResponse.internalServerError('Internal Server Error', ex));
                        }
                    } else {
                        res.json(ApiResponse.notAcceptable('Login fail!', {
                            message: 'invalid username or password! '
                        }));
                    }
                });
            } else {
                res.json(ApiResponse.notAcceptable('Login fail!', {
                    message: 'invalid username or password! '
                }));
            }
        });
    } else {
        res.json(ApiResponse.unAuthorized('Login fail!', {
            message: 'Authorization not found!'
        }));
    }
});

router.get('/', (req, res) => {
    let token = req.body.token || req.headers.authorization || req.cookies['x-access-token'];

    if (token) {
        try {
            token = token.replace('Bearer ', '');
            Jwt.verify(token, config.secretKey, (err, decoded) => {
                if (err) {
                    return res.json(ApiResponse.unAuthorized('Invalid token', err));
                } else {
                    return res.json(ApiResponse.ok('Token valid!', decoded));
                }
            });
        } catch (ex) {
            return res.json(ApiResponse.unAuthorized('Invalid token', ex));
        }
    } else {
        res.json(ApiResponse.unAuthorized('Token not found', {
            message: 'Unauthorized!'
        }));
    }
});

module.exports = router;