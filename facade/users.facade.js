var express = require('express');
var Sequelize = require('sequelize');
var async = require('async');
var ApiResponse = require('../model/apiresponse');
var Users = require('../model/users');
var validate = require('express-validation');
var Joi = require('joi');
var reqvalid = require("../validation/request-validation");
var modelvalid = require("../validation/model-validation");
var bcrypt = require('bcryptjs');

var router = express.Router();

router.get('/', (req, res) => {
    Users.findAll().then((res2) => {
        res.json(ApiResponse.ok('ok bos', res2));
    });
});

router.get('/:id', validate({
    params: {
        id: Joi.number().required()
    }
}), (req, res) => {
    Users.findById(req.params.id).then((res2) => {
            if (res2) {
                res.json(ApiResponse.ok('ok bos', res2));
            } else {
                res.json(ApiResponse.notFound());
            }
        },
        (err) => {
            res.json(ApiResponse.unknownError('error', err));
        });
});

router.post('/paging', validate(reqvalid.page), (req, res, next) => {
    var criterias = req.body.criteria || [];
    var where = [];
    var str = "";
    criterias.forEach((item, i) => {
        str = `{"${item.criteria}":{"$like":"%${item.value}%"}}`;
        where.push(JSON.parse(str));
    });
    let page = req.body.page < 1 ? 1 : req.body.page;
    let start = (page - 1) * req.body.pageSize;

    async.parallel({
        rows: (callback) => {
            Users.findAll({
                where: where,
                offset: start,
                limit: req.body.pageSize,
                order: [
                    [req.body.order.column, req.body.order.direction]
                ]
            }).then(rowData => {
                callback(null, rowData);
            }).catch(err => {
                res.json(ApiResponse.unProcessableEntity(err));
            });
        },
        rowCount: (callback) => {
            Users.count({
                where: where
            }).then(rowCount => {
                callback(null, rowCount);
            });
        }
    }, (err, result) => {
        if (err) {
            res.json(ApiResponse.notAcceptable(err, req.body));
        } else {
            result['pageCount'] = Math.ceil(result.rowCount / parseFloat(req.body.pageSize));;
            res.json(ApiResponse.ok('ok', result));
        }
    });

});

router.post('/', validate(modelvalid.users), (req, res) => {
    // Set has bycrypt
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(req.body.Password, salt, (err2, hash) => {
            // Store hash in your password DB.
            req.body['Hash'] = hash;
            Users.create(req.body).then((res2) => {
                res.json(ApiResponse.created('ok'));
            }, (error) => {
                res.json(ApiResponse.unProcessableEntity('insert fail', error.errors));
            });
        });
    });
});

router.put('/', validate(modelvalid.users), (req, res) => {
    Users.findById(req.body.UserID).then(
        (res2) => {
            if (res2) {
                res2.update(req.body).then((res3) => {
                    res.json(ApiResponse.ok('ok'));
                }, (error) => {
                    res.json(ApiResponse.unProcessableEntity('unProcessable Entity', error));
                });
            } else {
                res.json(ApiResponse.notFound());
            }
        },
        (err) => {
            res.json(ApiResponse.unknownError('error', err));
        }
    );
});

router.delete('/:id', validate({
    params: {
        id: Joi.string().min(1).max(5).required()
    }
}), (req, res) => {
    Users.findById(req.params.id).then(
        (res2) => {
            if (res2) {
                res2.destroy().then((res3) => {
                    res.json(ApiResponse.ok('ok'));
                }, (error) => {
                    res.json(ApiResponse.unProcessableEntity('unProcessable Entity', error));
                });
            } else {
                res.json(ApiResponse.notFound());
            }
        },
        (err) => {
            res.json(ApiResponse.unknownError('error', err));
        }
    );
});

router.post('/count', validate(reqvalid.count), (req, res, next) => {
    var criterias = req.body.criteria || [];
    var where = [];
    var str = "";
    criterias.forEach((item, i) => {
        str = `{"${item.criteria}":{"$like":"%${item.value}%"}}`;
        where.push(JSON.parse(str));
    });

    Users.count({
        where: where
    }).then(res2 => {
        res.json(ApiResponse.ok('ok bos', res2));
    });
});

module.exports = router;