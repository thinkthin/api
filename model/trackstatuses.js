var Sequelize = require('sequelize');
var sequelize = require('../config/datasource');
var Tracks = require('./tracks');

var Trackstatuses = sequelize.define('trackstatuses', {
    TrackStatusID: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    TrackID: { type: Sequelize.INTEGER },
    TrackStatus: { type: Sequelize.STRING },
    Timestamp: { type: Sequelize.DATE, defaultValue: Sequelize.NOW() },
    Status: { type: Sequelize.BOOLEAN, defaultValue: 1 }
}, { timestamps: false });

module.exports = Trackstatuses;