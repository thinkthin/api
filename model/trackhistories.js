var Sequelize = require('sequelize');
var sequelize = require('../config/datasource');
var Tracks = require('./tracks');

var Trackhistories = sequelize.define('trackhistories', {
    TrackHistoryID: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    TrackID: { type: Sequelize.INTEGER },
    Latitude: { type: Sequelize.DECIMAL(8,6) },
    Longitude: { type: Sequelize.DECIMAL(9,6) },
    Timestamp: { type: Sequelize.DATE, defaultValue: Sequelize.NOW() },
    Status: { type: Sequelize.BOOLEAN, defaultValue: 1 }
}, { timestamps: false });

Trackhistories.belongsTo(Tracks, { targetKey: 'TrackID', foreignKey: 'TrackID' });
Tracks.hasMany(Trackhistories, { sourceKey: 'TrackID', foreignKey: 'TrackID'});

module.exports = Trackhistories;