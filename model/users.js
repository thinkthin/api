var Sequelize = require('sequelize');
var sequelize = require('../config/datasource');

var Users = sequelize.define('users', {
    UserID: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    Name: { type: Sequelize.STRING },
    Username: { type: Sequelize.STRING, validate: { is: ["^[a-zA-Z0-9]+$",'i'] } },
    Password: { type: Sequelize.STRING },
    Hash: { type: Sequelize.STRING },
    Role: { type: Sequelize.STRING },
    Timestamp: { type: Sequelize.DATE, defaultValue: Sequelize.NOW() },
    Status: { type: Sequelize.BOOLEAN, defaultValue: 1 }
}, { timestamps: false });

module.exports = Users;