var Sequelize = require('sequelize');
var sequelize = require('../config/datasource');
var Users = require('./users');

var Tracks = sequelize.define('tracks', {
    TrackID: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    UserID: { type: Sequelize.INTEGER },
    TrackName: { type: Sequelize.STRING },
    Timestamp: { type: Sequelize.DATE, defaultValue: Sequelize.NOW() },
    Status: { type: Sequelize.BOOLEAN, defaultValue: 1 }
}, { timestamps: false });

Tracks.belongsTo(Users, { targetKey: 'UserID', foreignKey: 'UserID' });
Users.hasMany(Tracks, { sourceKey: 'UserID', foreignKey: 'UserID'});

module.exports = Tracks;