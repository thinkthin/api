"use strict";

var config = require('./config/config');
var http = require('http'),
    express = require('express'),
    validate = require('express-validation');

var cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser');

var jwtAuth = require('./middleware/jwt.authorization'),
    cors = require('./middleware/cors'),
    errorhandle = require('./middleware/errorhandling');

var pretty = require('express-prettify');


var ApiResponse = require('./model/apiresponse'),
    authenticate = require('./facade/authenticate.facade'),
    users = require('./facade/users.facade'),
    tracks = require('./facade/tracks.facade'),
    trackhistories = require('./facade/trackhistories.facade'), 
    trackstatuses = require('./facade/trackstatuses.facade');  

var app = new express();

app.use(pretty({
    query: 'pretty'
}));
app.set('port', config.port);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());


//Enable CORS
app.use(cors);

//authentication route
app.use('/authenticate', authenticate);

//JWT Authorization Middleware
// app.use(jwtAuth);

// //App route
app.use('/users', users);
app.use('/tracks', tracks);
app.use('/trackhistories', trackhistories);
app.use('/trackstatuses', trackstatuses);

//Error handling
app.use(errorhandle);


http.createServer(app).listen(config.port, () => {
    console.log('Server run on port: ' + app.get('port'));
});