var ApiResponse = require('../model/apiresponse');
var config = require('../config/config.js');
var Jwt = require('jsonwebtoken');

module.exports = (req, res, next)=>{         
    let token = req.body.token || req.headers.authorization || req.cookies['x-access-token'];

    if(token){
        try{
            token = token.replace('Bearer ',''); 
            Jwt.verify(token, config.secretKey, (err, decoded)=>{
                if(err){
                    return res.json(ApiResponse.unAuthorized('Invalid token', err));
                }else{
                    req.decoded = decoded;
                    next();
                }
            });
        }catch(ex){
            return res.json(ApiResponse.unAuthorized('Invalid token', ex));
        }
    }else{        
        res.json(ApiResponse.unAuthorized('Token not found', {message: 'Unauthorized!'}));
    }
    
}