var ApiResponse = require('../model/apiresponse');

module.exports = (err, req, res, next) => {
    if (err) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
        res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        switch (err.status) {
            case 400:
                res.json(ApiResponse.badRequest(err.statusText, err.errors));
                break;
            case 500:
                res.json(ApiResponse.internalServerError(err.statusText, err.errors));
                break;
            default:
                res.json(ApiResponse.internalServerError(err.statusText, err));
        }
    }

}